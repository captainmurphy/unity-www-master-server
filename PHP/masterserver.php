<?php

$server = '127.0.0.1'; // mssql server address
$user = 'masterserver'; // mssql username
$pass = '1234abcd'; // mssql password
$db = 'MasterServer'; // mssql database name
$table = "MasterServerVersion"; // mssql name of the datatable in the database
$diff = 300; //amount of time to let a host listing 'live'

$error = ''; // generic error string that gets reused
$errorcode = 200; //current code to output
$query = ''; // generic query string that gets reused
$function = getRequest('function'); //what function should be performed

//attempt connection to the database
$mysqli = new mysqli($server, $user, $pass, $db) or response(json_encode(Array("RETURN"=>"could not connect")),500);
if ($mysqli->connect_errno) {
    $error = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    response(json_encode(Array("RETURN"=>($error))),500);
} else {
    $query = "CREATE TABLE IF NOT EXISTS " .$table. " (".
             "id bigint(20) NOT NULL AUTO_INCREMENT,".
             "guid varchar(256) NOT NULL,".
             "gameName varchar(256) NOT NULL,".
             "maxPlayers mediumint(16) NOT NULL,".
             "currentPlayers mediumint(16) NOT NULL,".
             "gameType varchar(256) NOT NULL,".
             "usePassword tinyint(1) NOT NULL,".
             "password varchar(256) NOT NULL,".
             "externalIP varchar(64) NOT NULL,".
             "externalPort smallint(8) NOT NULL,".
             "internalIP varchar(64) NOT NULL,".
             "internalPort smallint(8) unsigned NOT NULL,".
             "useNat tinyint(1) NOT NULL,".
             "sceneName varchar(256) NOT NULL,".
             "version varchar(256) NOT NULL,".
             "comment blob NOT NULL,".
             "timestamp DATETIME DEFAULT NULL,".
             "PRIMARY KEY (id),".
             "KEY guid_key (guid)".
             ") ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
    if (!$mysqli->query($query)) {
        response(json_encode(Array('ERROR'=>'Could not create table: (' . $mysqli->errno . ') ' . $mysqli->error)), 500);
    }
}

//variables that get passed in
$id = getRequest('id'); //individual row ID, auto incrementing
$guid = (getRequest('guid') != '') ? getRequest('guid') : md5(uniqid(rand(), true)); //unique hash created for each registered server. auto fill with hash if blank otherwise use the passed in value
$guidIn = getRequest('guid'); //used for update/unregister
$gameName =  rawurlencode(getRequest('gamename'));  //game name
$maxPlayers = getRequest('maxplayers'); //maximum number of players for host
$currentPlayers = getRequest('currentplayers'); //current number of conected players
$gameType = rawurlencode(getRequest('gametype')); //string for game type
$usePassword = (getRequest('usepassword') != '') ? '1' : '0'; //either it has data or not, don't care and will make it pw protected later
$password = rawurlencode(getRequest('password')); // password used for protecting connection information
$externalIP = (getRequest('externalip') != '') ? getRequest('externalip') : getClientIP();
$externalPort = getRequest('externalport');
$internalIP = getRequest('internalip'); //internal IP of host. Used for LAN connections
$internalPort = (getRequest('internalport') != '') ? getRequest('internalport') : getRequest('externalport'); //internal port if different than external port
$nat = (getRequest('usenat') != '') ? '1' : '0'; //use nat translation - not yet working in wakenet but added for other systems
$comment = rawurlencode(getRequest('comment')); //comment about game
$scenename = rawurlencode(getRequest('scenename')); //name of the scene the host is running
$version = rawurlencode(getRequest('version'));

//filter values
$showprivate = getRequest('showprivate');

//// database structure
//// id, guid, gameName, maxPlayers, currentPlayers, gameType, passwordProtected, password, externalIP, externalPort,
////   internalIP, internalPort, useNat, comment, timestamp
//test url - Register a server
// masterserver.php?function=register&gamename=Game%20Name&maxplayers=8&currentplayers=1&gametype=Deathmatch&usepassword=1&password=11234abcd&externalip=123.123.123.123&externalport=8888&internalip=192.168.1.100&internalport=8889&usenat=0&comment=no%20comment

/// FOR TESTING - TODO REMOVE BEFORE RELEASE
if ($function == 'clear') {
    $query = "DELETE FROM ".$table." WHERE id > 0;";
    if (!$mysqli->query($query)) {
        $error = "Failed execute query: (" . $mysqli->errno . ") " . $mysqli->error;
        $errorcode = 500;
    }
    response(json_encode(Array("RETURN"=>"table cleared")));
}


/// Remove this before going live. If you plan to use this, make sure your user has DROP permissions enabled.
if ($function == 'drop') {
    $query = "DROP TABLE IF EXISTS ".$table.";";
    if (!$mysqli->query($query)) {
        $error = "Failed execute query: (" . $mysqli->errno . ") " . $mysqli->error;
        $errorcode = 500;
    }
   response(json_encode(Array("RETURN"=>"table dropped: ".$error)),$errorcode);
}

if ($function == 'all') {
    $query = "SELECT * FROM ".$table." WHERE id > 0;";
    if (!$mysqli->query($query)) {
        $error = json_encode(Array("ERROR"=>"Failed execute query: (" . $mysqli->errno . ") " . $mysqli->error));
        response($error,500);
    }
        $res = $mysqli->query($query);
        
        $rows = array();
        while($r = mysqli_fetch_assoc($res)) {
            $rows[] = $r;
        }
        response(json_encode($rows, JSON_NUMERIC_CHECK));
}

if ($function == 'getexternalip') {
    response('{"externalIP":"'.getClientIP().'"}');
}

if ($function == 'register') {
    //register - register a new server in the database
    if ($nat == '' || $gameType == '' || $gameName == '' || $currentPlayers == '' || $maxPlayers == ''
            || $internalIP == '' || $internalPort == '' || $externalIP == '' || $externalPort == '' || $usePassword == '') {
        $error = "ERROR: All fields not filled out. Please correct and resubmit";
        $errorcode = 400;
    } else {
        $query = "INSERT INTO ".$table.
          " (guid, gameName, maxPlayers, currentPlayers, gameType, usePassword, password, externalIP, externalPort,".
          " internalIP, internalPort, useNat, sceneName, version, comment, timestamp) VALUES ".
          "('".$guid."','".$gameName."',".$maxPlayers.",".$currentPlayers.",'".$gameType."',".$usePassword.",'".$password.
          "','".$externalIP."',".$externalPort.",'".$internalIP."',".$internalPort.",".$nat.",'".$scenename."','".$version."','".$comment."',NOW());";
        if (!$mysqli->query($query)) {
            $error = "Failed to add server entry: (" . $mysqli->errno . ") " . $mysqli->error;
            $errorcode = 500;
        } else {
            response('{"guid":"'.$guid.'","externalIP":"'.$externalIP.'"}');
        }
    }
} else if ($function == 'query' || $function == 'playercount') {
    //query - looks for a server with certain filters, runs a cleanup on the list before returning data
    if ($gameType == '') {
        $error = "ERROR: All fields not filled out. Please correct and resubmit";
        $errorcode = 400;
    } else {

        $playercount = 0;
        $servercount = 0;
     
        //clear old server entries that haven't updated recently
        $query = "DELETE FROM ".$table." WHERE TIME_TO_SEC(TIMEDIFF(NOW(),timestamp)) > ".$diff.";";
        if (!$mysqli->query($query)) {
            $error = "Failed execute query: (" . $mysqli->errno . ") " . $mysqli->error;
            $errocode = 500;
        } else {
            $where = " WHERE ";
            if ($showprivate == '') {
                $showprivate = " usePassword = 0 ";
            } else {
                $showprivate = "";
            }
            if ($gameName != '') {
                $gameName = " gameName = '" .$gameName."' ";
            }
            if ($gameType == 'ALL') {
                $gameType = "";
            } else {
                $gameType = " gameType = '".$gameType."'";
            }
            if ($gameType == "" && $showprivate == "") {
                $where = "";
            }
            //if there is a password on the server, return a 0.0.0.0 for IP. Only give IP after password verification step
            $query = "SELECT id,gameName,maxPlayers,currentPlayers,gameType,usePassword,externalIP,externalPort,internalIP,internalPort,useNat,sceneName,version,comment,guid, TIME_TO_SEC(TIMEDIFF(NOW(),timestamp)) as Time FROM ".$table.
                    (($gameType == "" && $showprivate == "" && $gameName == "") ? "" : " WHERE ").
                    $gameType.
                    (($gameType != "" && $showprivate != "") ? " AND " : ""). 
                    $showprivate.
                    (($showprivate != "" && $gameName != "") ? " AND " : "").
                    $gameName.
                    " ORDER BY guid ASC;";            
            $res = $mysqli->query($query);

            $rows = array();
            while($r = mysqli_fetch_assoc($res)) {
                $servercount++;
                $playercount += $r["currentPlayers"];    

                if ($r["usePassword"] == 1) {
                    $r["externalIP"] = 0;
                    $r["externalPort"] = 0;
                    $r["internalIP"] = 0;
                    $r["internalPort"] = 0;                   
                }

                //$r["password"] = "";
                //$r["guid"] = "";
                $rows[] = $r;
            }
            if ($function == 'query') {
                response('{"hosts": ' .json_encode($rows, JSON_NUMERIC_CHECK).'}',200);
            }
            if ($function == 'playercount') {
                response('{"playercount": '.$playercount.',"servercount":'.$servercount.'}',200);
            }
        }
    }        
} else if ($function == 'unregister') {
    //unregister - remove a server from the database
    if ($guidIn == '') {
         $error = "ERROR: All fields not filled out. Please correct and resubmit";
         $errorcode = 400;
    } else {
        $query = "DELETE FROM ".$table." WHERE guid = '".$guidIn."';";
        if (!$mysqli->query($query)) {
            $error = "Failed to remove server entry: (" . $mysqli->errno . ") " . $mysqli->error;
            $errorcode = 500;
        } else {
            response(json_encode(Array("RETURN"=>"OK")));
        }
    }
} else if ($function == 'updatehost') {
    //updatehost - update the host, pinging to keep it alive in the db, updating current number of players
    if ($guidIn == '') {
         $error = "ERROR: All fields not filled out. Please correct and resubmit";
         $errorcode = 400;
    } else {
        if ($currentPlayers == '') {
            $query = "UPDATE ".$table." SET timestamp = NOW() WHERE guid = '".$guidIn."';";
        } else {
            $query = "UPDATE ".$table." SET timestamp = NOW(), currentPlayers = ".$currentPlayers." WHERE guid = '".$guidIn."';";
        }
        if (!$mysqli->query($query)) {
            $error = "Failed to update server entry: (" . $mysqli->errno . ") " . $mysqli->error;
            $errorcode = 500;
        } else {
            response(json_encode(Array("RETURN"=>"OK")));
        }
    }
} else if ($function == 'passwordverify') {
    //passwordverify - get the IP of a server if the password is the same
    //query - looks for a server with certain filters, runs a cleanup on the list before returning data
    if ($id == '') {
         $error = "ERROR: All fields not filled out. Please correct and resubmit";
         $errorcode = 400;
    } else {
        $query = "SELECT id,gameName,maxPlayers,currentPlayers,gameType,usePassword,externalIP,externalPort,internalIP,internalPort,useNat,comment, TIME_TO_SEC(TIMEDIFF(NOW(),timestamp)) as Time FROM ".$table." WHERE id = '".$id."' AND usePassword = '1' AND password = '".$password."';";
        $res = $mysqli->query($query);
        
        $rows = array();
        while($r = mysqli_fetch_assoc($res)) {
            $rows[] = $r;
        }
        response(json_encode($rows, JSON_NUMERIC_CHECK));
    }
} 

//if no error occurred during sql build, perform the query
if ($error != "") {
    response(json_encode(Array("ERROR"=>$error)),$errorcode);
}

$mysqli->close();

function getRequest($requested) {
    return (isset($_REQUEST[$requested])) ? $_REQUEST[$requested] : '';
}

 function getClientIP() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function requestStatus($code) {
    if ($code == null) {
        $code = 500;
    }
    $status = array(
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted (Request accepted, and queued for execution)',
        400 => 'Bad Request',
        401 => 'Authentication failure',
        403 => 'Forbidden',
        404 => 'Resource Not Found',
        405 => 'Method Not Allowed',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        503 => 'Service Unavailable', 
    );
    return ($status[$code])?$status[$code]:$status[500];
}

function response($data, $status = 200) {
    header("HTTP/1.1 " . $status . " " . requestStatus($status));
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
    header("Content-Type: application/json");
    die($data); //only one thing should output at a time
}

?>